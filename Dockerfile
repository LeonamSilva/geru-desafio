FROM frolvlad/alpine-python3

MAINTAINER Leonam Silva

ENV HOME=/home/geru

WORKDIR $HOME/src/

COPY . $HOME/src/

WORKDIR $HOME/src/quoteApi 

RUN pip install -e .

WORKDIR $HOME/src/geruWeb

RUN pip install -e .

EXPOSE 6543

CMD ["pserve", "development.ini", "--reload"]