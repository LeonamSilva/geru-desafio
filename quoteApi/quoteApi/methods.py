import requests

URL = 'https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/quotes/{}'

def doRequest(number=-1):
    return requests.get(URL.format(number if number>=0 else '')).json()

def get_quote(number):
    try:
        number = int(number)
    except ValueError:
        raise Exception("Valor {} nao é um numero".format(number))
    response = doRequest(number=number)
    return response.get('quote', 'Citação Inexistente')

def get_quotes():
    response = doRequest()
    return response.get('quotes')


