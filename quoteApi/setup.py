from setuptools import setup, find_packages

requires = [
    'requests',
]

setup(name='quoteApi',
    version='1.0',
    description='lib que pega quotes na api da geru',
    author='Leonam Silva',
    author_email='leonam.silva@live.com',
    install_requires=requires,
    packages=find_packages()
)