# DESAFIO GERU #

### Instalando ###

Projeto pequeno para concorrer a uma cadeira na *Geru*

Para Executar o projeto instale o docker no seu sistema, dê preferencia ao Linux
A digital Ocean possui um [tutorial](https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-16-04-pt) sobre Docker no Ubuntu aqui

é hora de buildar o conteiner:
`docker build -t desafio-geru .`

depois voce pode executa-lo com:
`docker run -d --p 6543:6543 desafio-geru`

### Acessando o projeto ###

No navegador acesse:
```
http://localhost:6543/ -> Pagina Inicial

http://localhost:6543/quotes/[n° da citação] -> Citação especifica

http://localhost:6543/quotes/random -> Citação Aleátoria

http://localhost:6543/quotes -> Todas as citações

```

### ACESSANDO A API ###

Para acessar a api de consulta basta acessar os endereços abaixo:

```
http://localhost:6543/api/ -> Para Acessar Todas

http://localhost:6543/api/search_id/1 -> Para acessar um registro especifico
```


Autor: Leonam Silva
E-mail: leonam.silva@live.com
