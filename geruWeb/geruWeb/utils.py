import uuid
from pathlib import Path
from datetime import datetime

def new_hash():
    return uuid.uuid4().hex

PROJECT_FOLDER = Path('.').resolve().parents[0]
SQLITE_URI = PROJECT_FOLDER.parents[1].joinpath('geru.db')
WELCOME_MESSAGE = 'Desafio Geru Web 1.0'
MAX_QUOTES = 18