from datetime import datetime
from geruWeb.models import pipeline_database, SessionRegistry
from geruWeb.utils import new_hash

def session_register(fn):
    def _session_register(*args, **kwargs):
        request = args[1]
        session = request.session
        if session.new: 
            new_session_id = new_hash()
            session['session_id'] = new_session_id
        session_params = dict(date_created = datetime.fromtimestamp(session.created), 
                                requested_url = request.path, 
                                session_id = session['session_id'])
        pipeline_database.process_obj(SessionRegistry(**session_params))
        return fn(request)
    return _session_register