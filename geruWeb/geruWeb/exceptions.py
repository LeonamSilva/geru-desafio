import traceback
from pyramid.view import view_config

@view_config(context=Exception, renderer='500.mako')
def error_view(exc, request):
    traceback.print_exc()
    return {}