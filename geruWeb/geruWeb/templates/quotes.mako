<%inherit file="index.mako" />

<%block name="content">
    <div class="col-xs-6">
        <ul class="list-group">
            % for quote in quotes:
                <li class="list-group-item">${quote}</li>
            % endfor
        </ul>
    </div>
</%block>