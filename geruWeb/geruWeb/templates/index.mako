<html>
    <head>
        <link href="${request.static_url('geruWeb:static/bootstrap.min.css')}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <h3>${greeting_message}</h3>
                </div>
            </div>
            <div class="row">
                <%block name="content"/>
            </div>
        </div>
    </body>
</html>