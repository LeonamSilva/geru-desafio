from pyramid.config import Configurator
from pyramid.view import view_config
from pyramid.session import SignedCookieSessionFactory
from geruWeb.quotes import add_quote_routes
from geruWeb.api import add_api_routes
from geruWeb.utils import WELCOME_MESSAGE

@view_config(route_name='home', renderer='index.mako')
def home(request):
    return {'greeting_message': WELCOME_MESSAGE}

def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include('pyramid_mako')
    config.add_route('home', '/')
    config.include(add_quote_routes, route_prefix='/quotes')
    config.include(add_api_routes, route_prefix='/api')
    config.add_static_view('static', 'geruWeb:static', cache_max_age=3600)
    config.scan()
    config.set_session_factory(SignedCookieSessionFactory('hus7rururk2ii'))
    return config.make_wsgi_app()