import traceback
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from geruWeb.utils import SQLITE_URI

DeclarativeBase = declarative_base()

def db_connect():
    return create_engine('sqlite:///{}'.format(SQLITE_URI), echo=True)

def create_tables(engine):
    DeclarativeBase.metadata.create_all(engine)


class GeruPipeLine(object):
    def __init__(self):
        engine = db_connect()
        create_tables(engine)
        self.Session = sessionmaker(bind=engine)

    def process_obj(self, obj):
        session = self.Session()
        try:
            session.add(obj)
            session.commit()
        except:
            session.rollback()
            traceback.print_exc()
        finally:
            session.close()

    def get_all(self, cls):
        session = self.Session()
        return session.query(cls).all()

    def get_by_id(self, cls, id):
        session = self.Session()
        return session.query(cls).get(id)

from .models import *

pipeline_database = GeruPipeLine()

