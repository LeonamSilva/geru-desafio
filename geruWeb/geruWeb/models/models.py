from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from geruWeb.models import DeclarativeBase


__all__ = ['SessionRegistry']

class SessionRegistry(DeclarativeBase):
    __tablename__ = 'session_registry'

    def __init__(self, *args, **kwargs):
        self.session_id = kwargs['session_id']
        self.requested_url = kwargs['requested_url']
        self.date_created = kwargs['date_created']
    
    id = Column(Integer, primary_key=True)
    session_id = Column(String(50), nullable=False)
    requested_url = Column(String(50), nullable=False)
    date_created = Column(DateTime(), nullable=False)

    def __json__(self, request):
        return {'id':self.id, 
            'session':self.session_id, 
            'requested_url':self.requested_url, 
            'created': self.date_created.strftime("%Y-%m-%d %H:%M"), 
            'uri': '{}/api/search_id/{}'.format(request.host_url, self.id)
            }