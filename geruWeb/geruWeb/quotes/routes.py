__all__ = ['add_quote_routes']

def add_quote_routes(config):
    config.add_route('quotes', '/')
    config.add_route('quote', '/{number:\d+}')
    config.add_route('random', '/random')