from random import randint
from pyramid.view import view_config
from quoteApi import get_quote, get_quotes
from geruWeb.utils import WELCOME_MESSAGE, MAX_QUOTES
from geruWeb.decorators import session_register

__all__ = ['quotes', 'quote', 'random']

@view_config(route_name='quotes', renderer='quotes.mako')
@session_register
def quotes(request):
    quotes = get_quotes()
    return {'greeting_message': WELCOME_MESSAGE, 'quotes': quotes}
 
@view_config(route_name='quote', renderer='quote.mako')
@session_register
def quote(request):
    number = request.matchdict['number']
    quote = get_quote(number)
    return {'greeting_message': WELCOME_MESSAGE, 'quote_fetched': quote}

@view_config(route_name='random', renderer='quote.mako')
@session_register
def random(request):
    number = randint(0, MAX_QUOTES)
    quote = get_quote(number)
    return {'greeting_message': WELCOME_MESSAGE, 'quote_fetched': quote}
    