__all__ = ['add_api_routes']

def add_api_routes(config):
    config.add_route('all', '/')
    config.add_route('findById', '/search_id/{id:\d+}')