from pyramid.view import view_config, view_defaults
from geruWeb.models import SessionRegistry, pipeline_database as db


@view_defaults(route_name='all', renderer='json')
class SessionRegistryHomeView(object):
    
    def __init__(self, request):
        self.request = request 

    @view_config(request_method='GET')
    def get(self):
        retval = db.get_all(SessionRegistry)
        return retval


@view_defaults(route_name='findById', renderer='json')
class SessionRegistrySearchView(object):
    
    def __init__(self, request):
        self.request = request 

    @view_config(request_method='GET')
    def get(self):
        id = self.request.matchdict['id']
        retval = db.get_by_id(SessionRegistry, id)
        return retval