from setuptools import setup, find_packages

requires = [
    'pyramid',
    'waitress',
    'pyramid_mako',
    'plaster_pastedeploy',
    'sqlalchemy'
]

setup(name='geruWeb',
    version='1.3',
    description='cliente web',
    author='Leonam Silva',
    author_email='leonam.silva@live.com',
    install_requires=requires,
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'paste.app_factory': [
            'main = geruWeb:main',
        ],
    },
)